#pragma once
#include <QStyledItemDelegate>
#include <QTableView>

class MyItemDelegate : public QStyledItemDelegate
{
public:
	MyItemDelegate(const QTableView* tableView);
	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index);
private:
	const QTableView* m_tableView;
};
