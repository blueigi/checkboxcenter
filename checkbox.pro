#-------------------------------------------------
#
# Project created by QtCreator 2016-07-17T07:56:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = checkbox
TEMPLATE = app


SOURCES += main.cpp\
    mytablemodel.cpp \
    myitemdelegate.cpp

HEADERS  += \
    mytablemodel.h \
    myitemdelegate.h

FORMS    +=
