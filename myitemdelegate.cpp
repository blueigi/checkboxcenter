#include "myitemdelegate.h"
#include <QDebug>
#include <QEvent>
#include <QMouseEvent>


MyItemDelegate::MyItemDelegate(const QTableView *tableView)
	: m_tableView(tableView)
{

}

void MyItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyleOptionViewItem modifiedOption = option;

	if (index.column() == 0)
	{
		const int columnWidth	= m_tableView->columnWidth(index.column());
		const int columnMargin	= m_tableView->contentsMargins().left();
		const int checkboxWidth = option.decorationSize.width();
		const int moveValue = (columnWidth - checkboxWidth)/2 - columnMargin;

		QPoint oldCenter = option.rect.center();
		QPoint newCenter(oldCenter.x() + moveValue, oldCenter.y());
		modifiedOption.rect.moveCenter(newCenter);
	}
	QStyledItemDelegate::paint(painter, modifiedOption, index);
}

bool MyItemDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
	Qt::ItemFlags flags = model->flags(index);
	if (!(flags & Qt::ItemIsUserCheckable) || !(flags & Qt::ItemIsEnabled))
	{
		return false;
	}

	QVariant value = index.data(Qt::CheckStateRole);
	if (!value.isValid())
	{
		return false;
	}

	if (event->type() == QEvent::MouseButtonRelease)
	{
		const QMargins margins = QMargins(option.decorationSize.width()/2, option.decorationSize.height()/2, option.decorationSize.width()/2, option.decorationSize.height()/2);
		const QRect checkRect = QRect(option.rect.center(), QSize(0, 0)).marginsAdded(margins);
		if (!checkRect.contains(static_cast<QMouseEvent*>(event)->pos()))
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	Qt::CheckState state = (static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked ? Qt::Unchecked : Qt::Checked);
	return model->setData(index, state, Qt::CheckStateRole);
}
