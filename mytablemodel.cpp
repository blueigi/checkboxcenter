#include "mytablemodel.h"
#include <QDebug>

constexpr int ROW_VALUE = 10;

MyTableModel::MyTableModel()
	: m_checkedTable(new bool[ROW_VALUE])
{
	for (int i=0; i<ROW_VALUE; i++)
	{
		m_checkedTable[i] = false;
	}
}

int MyTableModel::rowCount(const QModelIndex &/*parent*/) const
{
	return ROW_VALUE;
}

int MyTableModel::columnCount(const QModelIndex &/*parent*/) const
{
	return 2;
}

QVariant MyTableModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) return QVariant();

	switch(role)
	{
		case Qt::DisplayRole:
			if (index.column() != 0)
			{
				return "inne";
			}
			break;
		case Qt::CheckStateRole:
			if (index.column() == 0)
			{
				return m_checkedTable[index.row()] ? Qt::Checked : Qt::Unchecked;
			}
		break;
	}
	return QVariant();
}

QVariant MyTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		return QString::number(section) + QString("c");
	}
	else if(orientation == Qt::Vertical && role == Qt::DisplayRole)
	{
		return QString::number(section) + QString("r");
	}


	return QVariant();
}

Qt::ItemFlags MyTableModel::flags(const QModelIndex &index) const
{
	return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
}

bool MyTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if(role == Qt::CheckStateRole)
	{
		if (value.toInt() == Qt::Checked)
		{
			m_checkedTable[index.row()] = true;
		}
		else
		{
			m_checkedTable[index.row()] = false;
		}
	}
	return true;
}
