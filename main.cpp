#include <myitemdelegate.h>
#include <mytablemodel.h>
#include <QApplication>
#include <QTableView>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	QTableView* tableView = new QTableView();


	tableView->setItemDelegate(new MyItemDelegate(tableView));
	tableView->setModel(new MyTableModel());

	tableView->setColumnWidth(0, 300);

	tableView->show();

    return a.exec();
}
